import UIKit

class ShopTableViewController: UITableViewController {
    @IBOutlet var shopTableView: UITableView!
    
    var categories: [Categories] = []
    var categor: [Category] = []
    var staff: [Goods] = []
    let id = "68"

//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let vc = segue.destination as? CategoriesTableViewController, segue.identifier == "ShowSubcategories" {
//            vc.id = "68"
//        }
//    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        shopTableView.delegate = self
//        shopTableView.dataSource = self
        
        CategoriesLoader().loadCategories {categories in
            self.categories = categories
        }
        
        CategoriesLdr().loadGoods(id: id) {goods in
            self.staff = goods
        }
        
        CategoriesLdr().loadCategories {categor in
            self.categor = categor
            self.shopTableView.rowHeight = 55
            self.shopTableView.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let cell = sender as? UITableViewCell, let index = tableView.indexPath(for: cell), let vc = segue.destination as? CategoriesTableViewController {
            vc.index = index.row
            print("Index segue = \(index.row)")
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int { return 1 }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categor.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShopTableViewCell", for: indexPath) as! ShopTableViewCell
        cell.categoryNameLabel.text = categor[indexPath.row].name
        let pictureURL = "https://blackstarwear.ru/\(categor[indexPath.row].imageURL)"
        cell.categoryLogoImageView.downloaded(from: pictureURL)
        return cell
    }
}

// MARK: - On-the-fly images loading source
extension UIImageView {
    func downloaded(from url: URL, contentMode mode: ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    func downloaded(from link: String, contentMode mode: ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}
