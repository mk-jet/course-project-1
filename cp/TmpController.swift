import UIKit

class TmpController: UIViewController {
    override var prefersStatusBarHidden: Bool { return true }

    override func viewDidLoad() {
        super.viewDidLoad()
        generateCategoriesTable()
        view.reloadInputViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    

    func generateCategoriesTable() {
        let topicImageView = UIImageView(frame: CGRect(x:0, y:1, width: view.frame.size.width, height:14))
        topicImageView.image = UIImage(named: "categoriesTopicLogoInvertedColors")
        topicImageView.contentMode = .center
        topicImageView.contentMode = .scaleAspectFit
        view.addSubview(topicImageView)
        
        
        
    }
}
