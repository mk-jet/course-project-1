import Foundation

class Category {
    let name: String
//    let sortOrder: Int
    let imageURL: String
    let subcategories: [Subcategory]
    
    init?(data: NSDictionary) {
        guard let name = data["name"] as? String,
//            let sortOrder = data["sortOrder"] as? String,
            let imageURL = data["iconImageActive"] as? String,
            let subcategories = data["subcategories"] as? [Subcategory] else {
                return nil
        }
            
        self.name = name
//        self.sortOrder = Int(sortOrder) ?? 0
        self.imageURL = imageURL
        self.subcategories = subcategories
    }
}
