import Foundation

class Goods {
    let name: String
//    let sortOrder: Int
//    let article: String
    let description: String
    let colorName: String
    let mainImage: String
    let productImages: NSArray
    let offers: NSArray
//    let recommendedProductIDs: [String]
    let price: String
//    let oldPrice: String
//    let tag: String
//    let attributes: NSArray
    
    
    init?(data: NSDictionary) {
        guard let name = data["name"] as? String,
              let description = data["description"] as? String,
              let colorName = data["colorName"] as? String,
              let mainImage = data["mainImage"] as? String,
              let productImages = data["productImages"] as? NSArray,
              let offers = data["offers"] as? NSArray,
              let price = data["price"] as? String else {
                return nil
        }
            
        self.name = name
        self.description = description
        self.colorName = colorName
        self.mainImage = mainImage
        self.productImages = productImages
        self.offers = offers
        self.price = price
    }
}
