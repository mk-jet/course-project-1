struct Subcategories: Decodable {
    var id: String
    var iconImage: String
    var sortOrder: Int
    var name: String
    var type: String
}
