import Foundation

class Subcategory {
    let id: String
    let iconImage: String
//    let sortOrder: Int
    let name: String
    let type: String
    
    init?(data: NSDictionary) {
        guard let id = data["id"] as? String,
              let iconImage = data["iconImage"] as? String,
//              let sortOrder = data["sortOrder"] as? String,
              let name = data["name"] as? String,
              let type = data["type"] as? String else {
                return nil
        }
            
        self.id = id
        self.iconImage = iconImage
//        self.sortOrder = Int(sortOrder) ?? 0
        self.name = name
        self.type = type
    }
}
