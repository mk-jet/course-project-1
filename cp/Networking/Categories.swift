struct Categories: Decodable{
    var name: String
    var sortOrder: Int
    var image: String
    var iconImage: String
    var iconImageActive: String
    var subcategories: [Subcategories]
}
