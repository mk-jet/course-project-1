struct CategoriesData: Decodable{
    var k67: Categories
    var k68: Categories
    var k69: Categories
    var k73: Categories
    var k74: Categories
    var k156: Categories
    var k165: Categories
    var k233: Categories
    var k0: Categories
    
    enum CodingKeys: String, CodingKey {
        case k67 = "67"
        case k68 = "68"
        case k69 = "69"
        case k73 = "73"
        case k74 = "74"
        case k156 = "156"
        case k165 = "165"
        case k233 = "233"
        case k0 = "0"
    }
}
