import UIKit

class ShopTableViewCell: UITableViewCell {
    
    @IBOutlet var categoryNameLabel: UILabel!
    @IBOutlet var categoryLogoImageView: UIImageView!

}
