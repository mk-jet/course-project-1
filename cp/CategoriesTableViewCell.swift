import UIKit

class CategoriesTableViewCell: UITableViewCell {

    @IBOutlet var subcategoryNameLabel: UILabel!
    @IBOutlet var subcategoryLogoImageView: UIImageView!
    
}
