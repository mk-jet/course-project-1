import Foundation
import Alamofire

// Site API basic data
let categoriesUrl = "https://blackstarshop.ru/index.php?route=api/v1/categories"
let categoriesListNumberss: [String] = ["k67", "k68", "k69", "k73", "k74", "k156", "k165", "k233", "k0"]
let categoriesListNumbers: [String] = ["67", "68", "69", "73", "74", "156", "165", "233", "0"]

class CategoriesLoader{
    func loadCategories(completion: @escaping ([Categories]) -> Void){
        var categoriesList: [Categories] = []
        AF.request (categoriesUrl).validate().responseDecodable(of: CategoriesData.self) { (response) in
            guard let allCategories = response.value else { return }
            var category: Categories = allCategories.k67
            categoriesList.append(category)
            category = allCategories.k68
            categoriesList.append(category)
            category = allCategories.k69
            categoriesList.append(category)
            category = allCategories.k73
            categoriesList.append(category)
            category = allCategories.k74
            categoriesList.append(category)
            category = allCategories.k156
            categoriesList.append(category)
            category = allCategories.k165
            categoriesList.append(category)
            category = allCategories.k233
            categoriesList.append(category)
            category = allCategories.k0
            categoriesList.append(category)
        }
            
        DispatchQueue.main.async {
            completion(categoriesList)
            print("Categories loaded by CategoriesLoader = \(categoriesList.count)")
        }
    }
}
