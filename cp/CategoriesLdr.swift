import Foundation
import Alamofire

class CategoriesLdr {
    let categoriesUrl = "https://blackstarshop.ru/index.php?route=api/v1/categories"
    let goodsUrl = "https://blackstarshop.ru/index.php?route=api/v1/products&cat_id="
    
    func loadCategories(completion: @escaping ([Category]) -> Void) {
            AF.request(categoriesUrl).responseJSON { response in
                if let objects = response.value,
                   let jsonDict = objects as? NSDictionary {
                    var categories: [Category] = []
                    for (_, data) in jsonDict where data is NSDictionary {
                        if let category = Category(data: data as! NSDictionary)
                            { categories.append(category) }
                    }
                    print("Categories loaded by CategoriesLdr = \(categories.count)")
                    DispatchQueue.main.async { completion(categories) }
                }
            }
    }
    
    
    func loadGoods(id: String, completion: @escaping ([Goods]) -> Void) {
        let url: String = goodsUrl+id
        print(url)
        AF.request(url).responseJSON { response in
            if let objects = response.value,
               let json = objects as? NSDictionary {
                var goods: [Goods] = []
                for (_, data) in json where data is NSDictionary {
                    if let stuff = Goods(data: data as! NSDictionary)
                    { goods.append(stuff) }
                }
                print("Goods loaded by CategoriesLdr = \(goods.count)")
                DispatchQueue.main.async { completion(goods) }
            }
        }
    }
}

