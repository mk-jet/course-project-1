import UIKit

class CategoriesTableViewController: UITableViewController {
    @IBOutlet var categoriesTableView: UITableView!
    
    var category: [Category] = []
    var index = 0
    var subcategories: [Subcategory] = []

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        categoriesTableView.delegate = self
//        categoriesTableView.dataSource = self

        print("Opening \(index)...")
        CategoriesLdr().loadCategories {category in
            self.category = category
        }
        subcategories = category[index].subcategories
        categoriesTableView.rowHeight = 55
        categoriesTableView.reloadData()
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int { return 1 }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        subcategories.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoriesTableViewCell", for: indexPath) as! CategoriesTableViewCell
        cell.subcategoryNameLabel.text = subcategories[indexPath.row].name
        let pictureURL = "https://blackstarwear.ru/\(subcategories[indexPath.row].iconImage)"
        cell.subcategoryLogoImageView.downloaded(from: pictureURL)
        return cell
    }
}
